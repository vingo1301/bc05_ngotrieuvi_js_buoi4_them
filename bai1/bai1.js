function ngayTiepTheo(){
    var ngay = document.getElementById("txt-ngay").value *1;
    var thang = document.getElementById("txt-thang").value *1;
    var nam = document.getElementById("txt-nam").value *1;
    if(((nam%4==0)&&(nam%100==1))||(nam%400==0)){
        if((ngay>=29) && (thang==2)){
            ngay = 1;
            thang = 3;
        } else if ((ngay >=31) && (thang==12)){
            ngay = 1;
            thang = 1;
            nam++;
        }
         else if( ngay == 30){
           switch(thang){
                case 4: ngay = 1;
                        thang ++;
                        break;
                case 6: ngay = 1;
                        thang ++;
                        break;
                case 9: ngay = 1;
                        thang ++;
                        break;
                case 11: ngay = 1;
                        thang ++;
                        break;
                default: ngay ++;
           }
        }
           else if (ngay >= 31){
                ngay = 1;
                thang ++;
           }
        else {
            ngay ++;
        }
    }
    else{
        if((ngay>=28) && (thang==2)){
            ngay = 1;
            thang = 3;
        } else if ((ngay >=31) && (thang==12)){
            ngay = 1;
            thang = 1;
            nam++;
        }
        else if( ngay == 30){
            switch(thang){
                 case 4: ngay = 1;
                         thang ++;
                         break;
                 case 6: ngay = 1;
                         thang ++;
                         break;
                 case 9: ngay = 1;
                         thang ++;
                         break;
                 case 11: ngay = 1;
                         thang ++;
                         break;
                 default: ngay ++;
            }
         } else if (ngay >= 31){
            ngay = 1;
            thang ++;
       }
        else {
            ngay ++;
        }
    }
    document.getElementById("result").innerHTML = `Ngày Tiếp Theo: ${ngay} Tháng: ${thang} Năm: ${nam}`;
}
function ngayTruocDo(){
    var ngay = document.getElementById("txt-ngay").value *1;
    var thang = document.getElementById("txt-thang").value *1;
    var nam = document.getElementById("txt-nam").value *1;
    if(((nam%4==0)&&(nam%100==1))||(nam%400==0)){
        if(ngay==1 && thang==3){
            ngay = 29;
            thang = 2;
        } else if (ngay ==1 && thang==1){
            ngay = 31;
            thang = 12;
            nam--;
        }
        else if(ngay == 1){
            switch(thang){
                case 5: ngay = 30;
                        thang --;
                        break;
                case 7: ngay = 30;
                        thang --;
                        break;
                case 10: ngay = 30;
                        thang --;
                        break;
                case 12: ngay = 30;
                        thang --;
                        break;
                case 2: ngay = 31;
                        thang --;
                        break;
                case 4: ngay = 31;
                        thang --;
                        break;
                case 6: ngay = 31;
                        thang --;
                        break;
                case 8: ngay = 31;
                        thang --;
                        break;
                case 9: ngay = 31;
                        thang --;
                        break;
                case 11: ngay = 31;
                        thang --;
                        break;
                default: ngay --;
            }
        }
        else {
            ngay --;
        }
    }
    else{
        if(ngay==1 && thang==3){
            ngay = 28;
            thang = 2;
        } else if (ngay ==1 && thang==1){
            ngay = 31;
            thang = 12;
            nam--;
        }
        else if(ngay == 1){
            switch(thang){
                case 5: ngay = 30;
                        thang --;
                        break;
                case 7: ngay = 30;
                        thang --;
                        break;
                case 10: ngay = 30;
                        thang --;
                        break;
                case 12: ngay = 30;
                        thang --;
                        break;
                case 2: ngay = 31;
                        thang --;
                        break;
                case 4: ngay = 31;
                        thang --;
                        break;
                case 6: ngay = 31;
                        thang --;
                        break;
                case 8: ngay = 31;
                        thang --;
                        break;
                case 9: ngay = 31;
                        thang --;
                        break;
                case 11: ngay = 31;
                        thang --;
                        break;
                default: ngay --;
            }
        }
        else {
            ngay --;
        }
    }
    document.getElementById("result").innerHTML = `Ngày Trước Đó: ${ngay} Tháng: ${thang} Năm: ${nam}`;
}